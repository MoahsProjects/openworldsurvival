// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "FPSCharacter.generated.h"

UCLASS()
class MYFIRSTPROJECT_API AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

private:
	bool bFirstPerson;
	bool bThirdPerson;

	USkeletalMeshComponent* ThirdPersonMesh;
	USpringArmComponent* SpringArm;

public:
	// Constructor for AFPSCharacter
	AFPSCharacter(const FObjectInitializer& ObjectInitializer);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UFUNCTION()
	void MoveForward(float value);

	UFUNCTION()
	void MoveRight(float value);

	UFUNCTION()
	void LookUp(float value);

	UFUNCTION()
	void Turn(float value);

	UFUNCTION()
	void OnStartJump();

	UFUNCTION()
	void OnStopJump();
	
	UFUNCTION()
	void OnStartCrouch();

	UFUNCTION()
	void OnStopCrouch();
	
	UFUNCTION()
	void OnCameraIn();

	UFUNCTION()
	void OnCameraOut();

	UFUNCTION()
	void OnFire();

	/** The drag speed of the third person camera */
	UPROPERTY(EditAnywhere, Category = Camera)
	float cameraLagSpeed;

	/** The distance of the third person camera */
	UPROPERTY(EditAnywhere, Category = Camera)
	float cameraLagMaxDist;

	/** The distance of the third person camera */
	UPROPERTY(EditAnywhere, Category = Camera)
	float cameraDistance;

	/** First Person Camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* FirstPersonCameraComponent;

	/** Third Person Camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	UCameraComponent* ThirdPersonCameraComponent;

	/** Pawn Mesh FirstPerson Only */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	USkeletalMeshComponent* FirstPersonMesh;

	/** Guns muzzle offset from camera */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	FVector MuzzleOffset;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class AFPSProjectile> ProjectileClass;
};
