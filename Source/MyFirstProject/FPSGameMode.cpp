// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFirstProject.h"
#include "FPSGameMode.h"
#include "FPSHUD.h"


AFPSGameMode::AFPSGameMode(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	// Set the defauklt pawn class to Blueprint character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnObject(TEXT("Pawn'/Game/Blueprints/BP_FPSCharacter.BP_FPSCharacter_C'"));
	if (PlayerPawnObject.Class != NULL) {
		DefaultPawnClass = PlayerPawnObject.Class;
	}

	HUDClass = AFPSHUD::StaticClass();
}

void AFPSGameMode::StartPlay() {
	Super::StartPlay();

	StartMatch();

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, TEXT("Hello World"));
	}
}