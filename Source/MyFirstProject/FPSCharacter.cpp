// Fill out your copyright notice in the Description page of Project Settings.

#include "MyFirstProject.h"
#include "FPSCharacter.h"
#include "FPSProjectile.h"

// Sets default values
AFPSCharacter::AFPSCharacter(const FObjectInitializer& ObjectInitializer)
	:Super(ObjectInitializer)
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bFirstPerson = true;
	bThirdPerson = !bFirstPerson;
	cameraLagSpeed = 2.f;
	cameraDistance = 450.f;
	cameraLagMaxDist = 100.f;

	//Create A FirstPerson Camera Component
	FirstPersonCameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->AttachParent = GetCapsuleComponent();

	SpringArm = ObjectInitializer.CreateDefaultSubobject<USpringArmComponent>(this, TEXT("CameraSmoothArm"));
	SpringArm->AttachParent = GetCapsuleComponent();
	SpringArm->RelativeRotation = FRotator(-45.f, 0.f, 0.f);
	SpringArm->RelativeLocation = FVector(0, 0, 50.f + BaseEyeHeight);
	SpringArm->CameraLagMaxDistance = 20.f;
	SpringArm->TargetArmLength = cameraDistance;
	SpringArm->bEnableCameraLag = true;
	SpringArm->CameraLagSpeed = cameraLagSpeed;

	// Create a third person camera component
	ThirdPersonCameraComponent = ObjectInitializer.CreateDefaultSubobject<UCameraComponent>(this, TEXT("ThirdPersonCamera"));
	ThirdPersonCameraComponent->AttachTo(SpringArm, USpringArmComponent::SocketName);

	//Position the camera
	FirstPersonCameraComponent->RelativeLocation = FVector(0, 0, 50.f + BaseEyeHeight);
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// A mesh Component That is only visible from FisrtPerson View
	FirstPersonMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("FirstPersonMesh"));
	FirstPersonMesh->SetOnlyOwnerSee(true);
	FirstPersonMesh->AttachParent = FirstPersonCameraComponent;
	FirstPersonMesh->bCastDynamicShadow = false;
	FirstPersonMesh->CastShadow = false;

	// everyone but the owner can see the regular body mesh
	ThirdPersonMesh = GetMesh();
	ThirdPersonMesh->SetOwnerNoSee(true);
}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (GEngine) {
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("THIS is The FPS Character"));
	}
	
}

// Called every frame
void AFPSCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);

	InputComponent->BindAxis("MoveForward", this, &AFPSCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &AFPSCharacter::MoveRight);
	InputComponent->BindAxis("Turn", this, &AFPSCharacter::Turn);
	InputComponent->BindAxis("LookUp", this, &AFPSCharacter::LookUp);

	InputComponent->BindAction("CameraIn", IE_Pressed, this, &AFPSCharacter::OnCameraIn);
	InputComponent->BindAction("CameraOut", IE_Pressed, this, &AFPSCharacter::OnCameraOut);

	InputComponent->BindAction("Jump", IE_Pressed, this, &AFPSCharacter::OnStartJump);
	InputComponent->BindAction("Jump", IE_Released, this, &AFPSCharacter::OnStopJump);

	InputComponent->BindAction("Crouch", IE_Pressed, this, &AFPSCharacter::OnStartCrouch);
	InputComponent->BindAction("Crouch", IE_Released, this, &AFPSCharacter::OnStopCrouch);
	
	InputComponent->BindAction("Fire", IE_Pressed, this, &AFPSCharacter::OnFire);
}

void AFPSCharacter::LookUp(float value) {

	if (bThirdPerson) {
		if (value != 0.f && Controller && Controller->IsLocalPlayerController()) {
			FRotator Rotation = SpringArm->RelativeRotation;
			Rotation.Pitch += -value;
			SpringArm->RelativeRotation = Rotation;
		}
	}
	else {
		AddControllerPitchInput(value);
	}
}

void AFPSCharacter::Turn(float value) {
	AddControllerYawInput(value);
}

void AFPSCharacter::MoveForward(float value) {
	if (value != 0.f && Controller && Controller->IsLocalPlayerController()) {
		// Get The forward rotation
		FRotator Rotation = Controller->GetControlRotation();
		// Limit Pitch whem waling of Falling
		if (GetCharacterMovement()->IsMovingOnGround() || GetCharacterMovement()->IsFalling()) {
			Rotation.Pitch = 0.0f;
		}

		//Add movement in that direction
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::X);
		AddMovementInput(Direction, value);

	}
}

void AFPSCharacter::MoveRight(float value) {
	if (value != 0.f && Controller && Controller->IsLocalPlayerController()) {
		const FRotator Rotation = Controller->GetControlRotation();
		const FVector Direction = FRotationMatrix(Rotation).GetScaledAxis(EAxis::Y);

		AddMovementInput(Direction, value);
	}
}

void AFPSCharacter::OnStartJump() {
	bPressedJump = true;
}

void AFPSCharacter::OnStopJump() {
	bPressedJump = false;
}

void AFPSCharacter::OnStartJump() {
	bPressedJump = true;
}

void AFPSCharacter::OnStopJump() {
	bPressedJump = false;
}

void AFPSCharacter::OnCameraIn() {
	if (bFirstPerson == false) {
		//APlayerController* Controller = Cast<APlayerController>(GetController());
		//Controller->SetViewTargetWithBlend(ThirdPersonCameraComponent, 0.7f);
		FirstPersonCameraComponent->Activate();
		ThirdPersonCameraComponent->Deactivate();
		bFirstPerson = true;
		bThirdPerson = !bFirstPerson;
		FirstPersonMesh->SetOwnerNoSee(!bFirstPerson);
		ThirdPersonMesh->SetOwnerNoSee(!bThirdPerson);
	}
}

void AFPSCharacter::OnCameraOut() {
	if (bThirdPerson == false) {
		FirstPersonCameraComponent->Deactivate();
		ThirdPersonCameraComponent->Activate();
		bFirstPerson = false;
		bThirdPerson = !bFirstPerson;
		FirstPersonMesh->SetOwnerNoSee(!bFirstPerson);
		ThirdPersonMesh->SetOwnerNoSee(!bThirdPerson);
	}
}

void AFPSCharacter::OnFire() {
	if (ProjectileClass != NULL) {
		// Get the camera location
		FVector CameraLoc;
		FRotator CameraRot;
		GetActorEyesViewPoint(CameraLoc, CameraRot);
		
		FVector const MuzzleLocation = CameraLoc + FTransform(CameraRot).TransformVector(MuzzleOffset);
		FRotator MuzzleRotation = CameraRot;
		MuzzleRotation.Pitch += 10.f; // Aim a bit upwards
		UWorld* const World = GetWorld();
		if (World) {
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.Instigator = Instigator;
			// Spawn the Projectile
			AFPSProjectile* const Projectile = World->SpawnActor<AFPSProjectile>(ProjectileClass, MuzzleLocation, MuzzleRotation, SpawnParams);
			if (Projectile) {
				// Find lauch direction
				FVector const LaunchDir = MuzzleRotation.Vector();
				Projectile->InitVelocity(LaunchDir);
			}
		}
	}
}